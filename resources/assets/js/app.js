const vendor = require ('./vendor');

(function () {
    $('.slick-slider').slick({
        slidesToScroll: 1,
        autoplay: 'auto',
    })
})();
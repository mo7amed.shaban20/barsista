const mix = require('laravel-mix');
const exec = require('child_process').exec;

mix.setResourceRoot('../');
mix.setPublicPath('public');
mix.copyDirectory('resources/assets/images', 'public/images');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/scss/app.scss', 'public/css', {
       outputStyle: 'compressed', /*  "expanded" or "compressed"  */
       sourceComments: true,
       autoprefixer: {
           options: {
               browsers: [
                   'last 6 versions',
               ]
           }
       }
   })
    .then(() => {
        exec('rtlcss public/css/app.css public/css/app_ar.css', (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return;
            }
            console.log(`stdout(RTLCSS): ${stdout}`);
            console.log(`stderr(RTLCSS): ${stderr}`);
        });
    });